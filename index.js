/*
* Note to Craig: So the initial code that I uploaded was pretty much hard coded, since I really sucks in JavaScript compared to Java.
* So I spend the rest of the evening trying to figure out how to NOT hard code, and well.. Some stuff succeeded, some did not.     
* Some of the earlier functions had a lot of console logging, so I could test if my JavaScript was okey. Do you think it is necessary
* to leave it like that? Look at getLoan() for example.
*/

let bankBalance = 0 //the user starts with 0.
let counter = 0 //counts the loan
let payBalance = 0 //the user starts with 0 pay
let choice = null //is used to keep track if which computer the user has chosen
let arrayNr // stores the arraynr so I could write buyComputer() better

//create class for Computer
class Computer{
    constructor(computerName, computerPrice, computerDesc, computerFeat, computerImage){
            this.computerName = computerName;
            this.computerPrice = computerPrice;
            this.computerDesc = computerDesc;
            this.computerFeat = computerFeat;
            this.computerImage = computerImage;
    }
}

let computerArray = [] //creates array

const dell = new Computer("Dell 1000", 1999, "This is a dell", "things a dell can do", "https://www.komplett.se/img/p/800/1138075.jpg")
const intel = new Computer("Intel X100", 599, "This is a intel", "things a intel can do", "https://static.turbosquid.com/Preview/001174/372/PK/DHQ.jpg")
const lenova = new Computer("Lenova Yoga", 399, "This is a lenova", "things a lenova can do", "https://www.elgiganten.se/image/dv_web_D180001002278505/47869/lenovo-yoga-c940-14-2-i-1-jaerngraa.jpg?$prod_all4one$")
const asus = new Computer("Asus Gaming Master", 5399, "This is a asus", "things a asus can do", "https://www.databyran.nu/images/products/dator/asus_x512ja-bq038t_001.jpg")

//stores the comp. in array
computerArray.push(dell)
computerArray.push(intel)
computerArray.push(lenova)
computerArray.push(asus)

//will print the names in the dropdown list. kinda hard coded, since you need to add a new row with id in html, but better than not having it
//
function load() { 
   for(i = 0; i < computerArray.length; i++) {
        document.getElementById(i).innerHTML = computerArray[i].computerName
    }   

}

//an attempt to write the HTML-code here, so that the code dont get as much hard coded as it was before
function loadComputer(computerArray , arrayNr) {
    document.getElementById('compImg').innerHTML = 
    `<img class="compImg" src="${computerArray.computerImage}"></img>`

    document.getElementById('compName').innerHTML = 
    `<b>${computerArray.computerName}</b>`

    document.getElementById('compPrice').innerHTML = 
    `<b>${computerArray.computerPrice} Kr</b>`

    document.getElementById('compDesc').innerHTML = 
    `<b>Description: </b> </br>${computerArray.computerDesc}`

    document.getElementById('compFeat').innerHTML = 
    `<b>Features:</b></br>${computerArray.computerFeat}`

    document.getElementById('compBuy').innerHTML = 
    `<button type="button" id="buyComputer" class="btn btn-info"onclick="buyComputer(${arrayNr})">BUY NOW</button>`
     
    choice = computerArray.computerName
}

//Lets the user try to loan with the parameters the task gave. Counter++ locks the alternative to do another loan.
function getLoan() {
    console.log("Get Loan")
    console.log(counter)
    let loanAmount = window.prompt("How much do you want to loan? Your balance is " + bankBalance, bankBalance)
    loanAmount = parseInt(loanAmount) //needed this to ensure loanAmount was an int

    if (loanAmount <= (bankBalance*2) && bankBalance > 0 && counter === 0) {
        console.log("Loan amount ok.")
        bankBalance = (bankBalance + loanAmount)
        document.getElementById("bankBalance").innerHTML = "Balance: " + bankBalance + " Kr.";
        counter++
    } else if (loanAmount > (bankBalance*2)) {
        console.log("Loan amount not ok.")
        alert("Loan amount not ok")
    } else if (counter > 0) {
        console.log("You have already a loan.")
        alert("You have already a loan.")
    } else if (bankBalance == 0) {
        console.log("You dont have any money")
        alert("You don't have any money")
    } else {
        console.log("You did something wrong. Did you type a letter?")
        alert("You did something wrong. Did you type a letter?")
    }
    console.log("--------------------------------")
}

//Transfer pay to bank, updates the variables and updates the information on the html site
function transferPay() {
    console.log("Transfer Pay to Bank")
    bankBalance = payBalance + bankBalance
    payBalance = 0
    document.getElementById("bankBalance").innerHTML = "Balance: " + bankBalance + " Kr.";
    document.getElementById("payBalance").innerHTML = "Pay: " + payBalance + " Kr.";
    console.log(payBalance + ", " + bankBalance)
    console.log("--------------------------------")
}

//lets the user "work". adds money to pay and updates it on the html page
function work() {
    console.log("Work")
    payBalance += 100
    document.getElementById("payBalance").innerHTML = "Pay: " + payBalance + " Kr.";
    console.log(payBalance)
    console.log("--------------------------------")
}

//based on what value the dropdownmenu have, you will get the corresponding computer in the complist. The problem here is that you can easy add a new computer without writing a new else if
function chooseComputer() {     
    if(document.getElementById("dropDownMenu").value == 0) {
        loadComputer(computerArray[0], 0)
        console.log(computerArray[0].computerName + " chosen")
    } else if (document.getElementById("dropDownMenu").value == 1) {
      loadComputer(computerArray[1], 1)
      console.log(computerArray[1].computerName + " chosen")
    } else if (document.getElementById("dropDownMenu").value == 2) {
        loadComputer(computerArray[2], 2)
        console.log(computerArray[2].computerName + " chosen")
    } else if (document.getElementById("dropDownMenu").value == 3) {
       loadComputer(computerArray[3], 3)
       console.log(computerArray[3].computerName + " chosen")
    }
}

//when you click the buy button, the function gets the computerArray[number] so it can make the function more easy to adjust
function buyComputer(arrayNr) {
    if(choice == computerArray[arrayNr].computerName && bankBalance >= computerArray[arrayNr].computerPrice) {
        bankBalance = bankBalance - computerArray[arrayNr].computerPrice
        document.getElementById("bankBalance").innerHTML = "Balance: " + bankBalance + " Kr.";
        alert("You now own a " + computerArray[arrayNr].computerName)
    } else {
        alert("Are you sure you have enough money?")
    }
}